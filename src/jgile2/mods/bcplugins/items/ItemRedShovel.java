package jgile2.mods.bcplugins.items;

import jgile2.mods.bcplugins.bcplugins;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.ItemSpade;

public class ItemRedShovel extends ItemSpade{

	public ItemRedShovel(int par1, EnumToolMaterial par2EnumToolMaterial) {
		super(par1, par2EnumToolMaterial);
		this.setCreativeTab(bcplugins.create);
		this.setUnlocalizedName("RedShovel");
	}
public void registerIcons(IconRegister iconRegister){
	itemIcon = iconRegister .registerIcon("bcplugins:RedShovel");
}
}
