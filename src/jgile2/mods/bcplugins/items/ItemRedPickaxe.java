package jgile2.mods.bcplugins.items;

import jgile2.mods.bcplugins.bcplugins;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.ItemPickaxe;


public class ItemRedPickaxe extends ItemPickaxe{

	public ItemRedPickaxe(int par1, EnumToolMaterial par2EnumToolMaterial) {
		super(par1, par2EnumToolMaterial);
		this.setCreativeTab(bcplugins.create);
		this.setUnlocalizedName("RedPickaxe");
	}

	public void registerIcons(IconRegister iconRegister) {
		this.itemIcon = iconRegister.registerIcon("bcplugins:RedPickaxe");
	}
}
