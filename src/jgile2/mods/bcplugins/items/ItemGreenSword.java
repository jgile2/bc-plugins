package jgile2.mods.bcplugins.items;

import jgile2.mods.bcplugins.bcplugins;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.ItemSword;

public class ItemGreenSword extends ItemSword {

	public ItemGreenSword(int par1, EnumToolMaterial par2EnumToolMaterial) {
		super(par1, par2EnumToolMaterial);
		this.setCreativeTab(bcplugins.create);
	}
	public void registerIcons(IconRegister iconRegister) {
		itemIcon = iconRegister .registerIcon("betertools:GreenSword");
	}
}