package jgile2.mods.bcplugins.items;

import jgile2.mods.bcplugins.bcplugins;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;


public class ItemRedDust extends Item{

	public ItemRedDust(int par1) {
		super(par1);
		this.setCreativeTab(bcplugins.create);
	}
	
	public void registerIcons(IconRegister iconRegister) {
		itemIcon = iconRegister .registerIcon("betertools:RedDust");
	}
	

}
