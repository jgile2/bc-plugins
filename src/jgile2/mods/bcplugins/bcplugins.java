/*
 * Copyright (C) 2012,2013 yogpstop
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the
 * GNU Lesser General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package jgile2.mods.bcplugins;

import java.lang.reflect.Field;

import jgile2.mods.bcplugins.TileEntity.TileBreaker;
import jgile2.mods.bcplugins.TileEntity.TileCharger;
import jgile2.mods.bcplugins.TileEntity.TileInfMJSrc;
import jgile2.mods.bcplugins.TileEntity.TileLaser;
import jgile2.mods.bcplugins.TileEntity.TileMarker;
import jgile2.mods.bcplugins.TileEntity.TileMiningWell;
import jgile2.mods.bcplugins.TileEntity.TilePlacer;
import jgile2.mods.bcplugins.TileEntity.TilePump;
import jgile2.mods.bcplugins.TileEntity.TileQuarry;
import jgile2.mods.bcplugins.TileEntity.TileRefinery;
import jgile2.mods.bcplugins.blocks.BlockBreaker;
import jgile2.mods.bcplugins.blocks.BlockCharger;
import jgile2.mods.bcplugins.blocks.BlockInfMJSrc;
import jgile2.mods.bcplugins.blocks.BlockLaser;
import jgile2.mods.bcplugins.blocks.BlockMarker;
import jgile2.mods.bcplugins.blocks.BlockMiningWell;
import jgile2.mods.bcplugins.blocks.BlockMover;
import jgile2.mods.bcplugins.blocks.BlockPipe;
import jgile2.mods.bcplugins.blocks.BlockPlacer;
import jgile2.mods.bcplugins.blocks.BlockPump;
import jgile2.mods.bcplugins.blocks.BlockQuarry;
import jgile2.mods.bcplugins.blocks.BlockRefinery;
import jgile2.mods.bcplugins.client.ClientProxy;
import jgile2.mods.bcplugins.items.ItemGreenPickaxe;
import jgile2.mods.bcplugins.items.ItemPhilosopherStone;
import jgile2.mods.bcplugins.items.ItemRedPickaxe;
import jgile2.mods.bcplugins.items.ItemRedShovel;
import jgile2.mods.bcplugins.items.ItemTool;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.StatCollector;
import net.minecraftforge.common.Configuration;
import net.minecraftforge.common.EnumHelper;
import net.minecraftforge.common.ForgeChunkManager;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.Property;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.world.WorldEvent;
import buildcraft.BuildCraftCore;
import buildcraft.BuildCraftSilicon;
import buildcraft.api.core.IIconProvider;
import buildcraft.transport.BlockGenericPipe;
import buildcraft.transport.ItemPipe;
import buildcraft.transport.Pipe;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.ObfuscationReflectionHelper;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

@Mod(modid = "bcplugins", name = "bcplugins", version = "@VERSION@", dependencies = "required-after:BuildCraft|Builders;required-after:BuildCraft|Core;required-after:BuildCraft|Energy;required-after:BuildCraft|Factory;required-after:BuildCraft|Silicon;required-after:BuildCraft|Transport")
@NetworkMod(clientSideRequired = true, serverSideRequired = false, channels = { PacketHandler.BTN, PacketHandler.NBT, PacketHandler.Tile, PacketHandler.Marker,"bcplugins" }, packetHandler = PacketHandler.class)
public class bcplugins {
	@SidedProxy(clientSide = "jgile2.mods.bcplugins.client.ClientProxy", serverSide = "jgile2.mods.bcplugins.CommonProxy")
	public static CommonProxy proxy;
	public static ClientProxy clientproxy;
	public int[] bid = null;
	public int[] iid = null;
	public static CreativeTabs create = new CreativeTab("BCPlugins");
	@Mod.Instance("bcplugins")
	public static bcplugins instance;
	public static final int refineryRenderID = RenderingRegistry.getNextAvailableRenderId();
	public static Block blockQuarry, blockMarker, blockMover, blockMiningWell, blockPump, blockInfMJSrc, blockRefinery, blockPlacer, blockBreaker, blockLaser,blockCharger;
	public static Item itemTool, RedPickaxe,RedAxe,RedSword,RedShovel,RedDust,RedIngot,RedBow,GreenPickaxe,GreenAxe,GreenSword,GreenShovel,PhilosopherStone;
	public static Item BlockPipe;
	public IIconProvider pipeIconProvider = new PipeIconProvider();
	
	public static EnumToolMaterial toolRedtool = EnumHelper.addToolMaterial("REDTOOL", 5, 8000, 15.0F, 8, 30);
	public static EnumToolMaterial toolGreentool = EnumHelper.addToolMaterial("GREENTOOL", 5, 3000, 15.0F, 5, 30);
	

	public static int RecipeDifficulty;

	public static Field redstoneChipsetF = null;
	static {
		try {
			redstoneChipsetF = BuildCraftSilicon.class.getField("redstoneChipset");
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
	}

	public static final int guiIdInfMJSrc = 1;
	public static final int guiIdMover = 2;
	public static final int guiIdFList = 3;
	public static final int guiIdSList = 4;
	public static final int guiIdPlacer = 5;
	public static final int guiIdPump = 7;
	public static final int guiIdCharger = 6;

	@ForgeSubscribe
	public void onWorldUnload(WorldEvent.Unload event) {
		TileMarker.Link[] la = TileMarker.linkList.toArray(new TileMarker.Link[TileMarker.linkList.size()]);
		for (TileMarker.Link l : la)
			if (l.w == event.world) l.removeConnection(false);
		TileMarker.Laser[] lb = TileMarker.laserList.toArray(new TileMarker.Laser[TileMarker.laserList.size()]);
		for (TileMarker.Laser l : lb)
			if (l.w == event.world) l.destructor();
	}

	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		Configuration cfg = new Configuration(event.getSuggestedConfigurationFile());
		
	
		try {
			cfg.load();
			bid = new int[] { 
					cfg.getBlock("Quarry", 1970).getInt(),
					cfg.getBlock("Marker", 1971).getInt(), 
					cfg.getBlock("EnchantMover", 1972).getInt(),
					cfg.getBlock("MiningWell", 1973).getInt(),
					cfg.getBlock("Pump", 1974).getInt(),
					cfg.getBlock("InfMJSrc", 1975).getInt(),
					cfg.getBlock("Refinery", 1976).getInt(), 
					cfg.getBlock("Placer", 1977).getInt(), 
					cfg.getBlock("Breaker", 1978).getInt(),
					cfg.getBlock("Laser", 1979).getInt(),
					cfg.getBlock("Pipe",1980).getInt(),
					cfg.getBlock("Charger",1981).getInt(),
					
			};
					
					
					
			iid = new int[]{
					cfg.getItem("RedPickaxe", 5000).getInt()-256,
					cfg.getItem("Tools", 18463).getInt()-256,
					cfg.getItem("RedShovel", 5001).getInt()-256,
					cfg.getItem("RedAxe", 5002).getInt()-256,
					cfg.getItem("RedSword", 5003).getInt()-256,
					cfg.getItem("Philosophers Stone", 5004).getInt()-256,
					cfg.getItem("GreenPickaxe", 5005).getInt()-256,
					
			};
			
			Property RD = cfg.get(Configuration.CATEGORY_GENERAL, "RecipeDifficulty", 2);
			RD.comment = "0:AsCheatRecipe,1:EasyRecipe,2:NormalRecipe(Default),3:HardRecipe,other:NormalRecipe";
			RecipeDifficulty = RD.getInt(2);
			PowerManager.loadConfiguration(cfg);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			cfg.save();
		}
		try {
			blockQuarry = (new BlockQuarry(bid[0]));
			blockMarker = (new BlockMarker(bid[1]));
			blockMover = (new BlockMover(bid[2]));
			blockMiningWell = (new BlockMiningWell(bid[3]));
			blockPump = (new BlockPump(bid[4]));
			blockInfMJSrc = (new BlockInfMJSrc(bid[5]));
			blockRefinery = (new BlockRefinery(bid[6]));
			blockPlacer = (new BlockPlacer(bid[7]));
			blockBreaker = (new BlockBreaker(bid[8]));
			blockLaser = (new BlockLaser(bid[9]));
			blockCharger = new BlockCharger(bid[11],Material.iron);
			itemTool = (new ItemTool(iid[1]));
			
			
			
			RedPickaxe = new ItemRedPickaxe(iid[0], toolRedtool);
			RedShovel = new ItemRedShovel(iid[2], toolRedtool);
			GreenPickaxe = new ItemGreenPickaxe(iid[6], toolGreentool);
			
			PhilosopherStone = new ItemPhilosopherStone(iid[5]);
				
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		LanguageRegistry.instance().loadLocalization("/lang/yogpstop/quarryplus/en_US.lang", "en_US", false);
		LanguageRegistry.instance().loadLocalization("/lang/yogpstop/quarryplus/ja_JP.lang", "ja_JP", false);
		ForgeChunkManager.setForcedChunkLoadingCallback(instance, new ChunkLoadingHandler());
		MinecraftForge.EVENT_BUS.register(this);
	}

	@Mod.EventHandler
	public void init(FMLInitializationEvent event) {
		
		GameRegistry.registerBlock(blockQuarry, ItemBlockQuarry.class, "QuarryPlus");
		GameRegistry.registerBlock(blockMarker, "MarkerPlus");
		GameRegistry.registerBlock(blockMover, "EnchantMover");
		GameRegistry.registerBlock(blockMiningWell, ItemBlockQuarry.class, "MiningWellPlus");
		GameRegistry.registerBlock(blockPump, ItemBlockPump.class, "PumpPlus");
		GameRegistry.registerBlock(blockInfMJSrc, "InfMJSrc");
		GameRegistry.registerBlock(blockRefinery, ItemBlockRefinery.class, "RefineryPlus");
		GameRegistry.registerBlock(blockPlacer, "PlacerPlus");
		GameRegistry.registerBlock(blockBreaker, ItemBlockBreaker.class, "BreakerPlus");
		GameRegistry.registerBlock(blockLaser, ItemBlockQuarry.class, "LaserPlus");
		GameRegistry.registerBlock(blockCharger,"BlockCharger");

		GameRegistry.registerTileEntity(TileQuarry.class, "QuarryPlus");
		GameRegistry.registerTileEntity(TileMarker.class, "MarkerPlus");
		GameRegistry.registerTileEntity(TileMiningWell.class, "MiningWellPlus");
		GameRegistry.registerTileEntity(TilePump.class, "PumpPlus");
		GameRegistry.registerTileEntity(TileInfMJSrc.class, "InfMJSrc");
		GameRegistry.registerTileEntity(TileRefinery.class, "RefineryPlus");
		GameRegistry.registerTileEntity(TilePlacer.class, "PlacerPlus");
		GameRegistry.registerTileEntity(TileBreaker.class, "BreakerPlus");
		GameRegistry.registerTileEntity(TileLaser.class, "LaserPlus");
		GameRegistry.registerTileEntity(TileCharger.class, "Charger");

		
		LanguageRegistry.addName(BlockPipe, "Item Sorter Pipe");
		
		Recipes r = new Recipes();
		r.assemblerRecipes(RecipeDifficulty);
		r.shaplessRecipes();
		r.shapedRecipes();
		NetworkRegistry.instance().registerGuiHandler(this, new GuiHandler());
		GameRegistry.registerCraftingHandler(new CraftingHandler());
		proxy.registerTextures();
		LanguageRegistry.instance().addStringLocalization("itemGroup.BCPlugins", "en_US", "BC Plugins");
	}

	public static String getname(short blockid, int meta) {
		StringBuffer sb = new StringBuffer();
		sb.append(blockid);
		if (meta != 0) {
			sb.append(":");
			sb.append(meta);
		}
		sb.append("  ");
		ItemStack cache = new ItemStack(blockid, 1, meta);
		if (cache.getItem() == null) {
			sb.append(StatCollector.translateToLocal("tof.nullblock"));
		} else if (cache.getDisplayName() == null) {
			sb.append(StatCollector.translateToLocal("tof.nullname"));
		} else {
			sb.append(cache.getDisplayName());
		}
		return sb.toString();
	}

	public static String getname(long data) {
		return getname((short) (data % 0x1000), (int) (data >> 12));
	}

	public static long data(short id, int meta) {
		return id | (meta << 12);
	}

	
	
	
}
