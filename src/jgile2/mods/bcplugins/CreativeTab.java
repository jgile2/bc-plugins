package jgile2.mods.bcplugins;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class CreativeTab extends CreativeTabs
{
    public CreativeTab(String label)
    {
        super(label);
    }
    @SideOnly(Side.CLIENT)

    public ItemStack getIconItemStack()
    {
        return new ItemStack(bcplugins.blockQuarry);
    }
}
