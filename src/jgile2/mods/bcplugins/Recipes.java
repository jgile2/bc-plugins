package jgile2.mods.bcplugins;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import buildcraft.BuildCraftBuilders;
import buildcraft.BuildCraftCore;
import buildcraft.BuildCraftEnergy;
import buildcraft.BuildCraftFactory;
import buildcraft.BuildCraftSilicon;
import buildcraft.BuildCraftTransport;
import buildcraft.api.recipes.AssemblyRecipe;
import cpw.mods.fml.common.registry.GameRegistry;

public class Recipes {

	public void assemblerRecipes(int dif){

		Item redstoneChipset = null;
		try {
			redstoneChipset = (Item) bcplugins.redstoneChipsetF.get(null);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		switch (dif) {
		case 0:
			GameRegistry.addRecipe(new ItemStack(bcplugins.blockMarker, 1),
					new Object[] { "X", "Y", Character.valueOf('Y'), BuildCraftBuilders.markerBlock, Character.valueOf('X'), Item.redstone });
			GameRegistry.addRecipe(new ItemStack(bcplugins.blockQuarry, 1),
					new Object[] { "X", "Y", Character.valueOf('Y'), BuildCraftFactory.quarryBlock, Character.valueOf('X'), Item.redstone });
			GameRegistry.addRecipe(new ItemStack(bcplugins.blockMover, 1), new Object[] { "X", "Y", Character.valueOf('Y'), BuildCraftFactory.autoWorkbenchBlock,
					Character.valueOf('X'), Item.redstone });
			GameRegistry.addRecipe(new ItemStack(bcplugins.blockMiningWell, 1), new Object[] { "X", "Y", Character.valueOf('Y'), BuildCraftFactory.miningWellBlock,
					Character.valueOf('X'), Item.redstone });
			GameRegistry.addRecipe(new ItemStack(bcplugins.blockPump, 1),
					new Object[] { "X", "Y", Character.valueOf('Y'), BuildCraftFactory.pumpBlock, Character.valueOf('X'), Item.redstone });
			GameRegistry.addRecipe(new ItemStack(bcplugins.blockRefinery, 1),
					new Object[] { "X", "Y", Character.valueOf('Y'), BuildCraftFactory.refineryBlock, Character.valueOf('X'), Item.redstone });
			GameRegistry.addRecipe(new ItemStack(bcplugins.itemTool, 1, 0),
					new Object[] { "X", "Y", Character.valueOf('Y'), BuildCraftCore.wrenchItem, Character.valueOf('X'), Item.sign });
			GameRegistry.addRecipe(new ItemStack(bcplugins.itemTool, 1, 1),
					new Object[] { "X", "Y", Character.valueOf('Y'), BuildCraftCore.wrenchItem, Character.valueOf('X'), Item.paper });
			GameRegistry.addRecipe(new ItemStack(bcplugins.itemTool, 1, 2),
					new Object[] { "X", "Y", Character.valueOf('Y'), BuildCraftCore.wrenchItem, Character.valueOf('X'), Item.bucketEmpty });
			GameRegistry.addRecipe(new ItemStack(bcplugins.blockBreaker, 1), new Object[] { "X", "Y", Character.valueOf('Y'), Block.dispenser, Character.valueOf('X'),
					Item.pickaxeIron });
			GameRegistry.addRecipe(new ItemStack(bcplugins.blockPlacer, 1), new Object[] { "X", "Y", Character.valueOf('Y'), Block.dispenser, Character.valueOf('X'),
					Item.redstone });
			GameRegistry.addRecipe(new ItemStack(bcplugins.blockLaser, 1),
					new Object[] { "X", "Y", Character.valueOf('Y'), BuildCraftSilicon.laserBlock, Character.valueOf('X'), Item.redstone });
			break;
		case 1:
			AssemblyRecipe.assemblyRecipes.add(new AssemblyRecipe(new ItemStack[] { new ItemStack(BuildCraftFactory.quarryBlock, 1),
					new ItemStack(redstoneChipset, 1, 3), new ItemStack(BuildCraftTransport.yellowPipeWire, 8) }, 160000, new ItemStack(bcplugins.blockQuarry, 1)));
			AssemblyRecipe.assemblyRecipes.add(new AssemblyRecipe(new ItemStack[] { new ItemStack(Block.enchantmentTable, 1),
					new ItemStack(BuildCraftFactory.autoWorkbenchBlock, 1), new ItemStack(Block.anvil, 1), new ItemStack(BuildCraftSilicon.laserBlock, 2),
					new ItemStack(BuildCraftSilicon.assemblyTableBlock, 1) }, 40000, new ItemStack(bcplugins.blockMover, 1)));
			AssemblyRecipe.assemblyRecipes.add(new AssemblyRecipe(new ItemStack[] { new ItemStack(BuildCraftBuilders.markerBlock, 1),
					new ItemStack(redstoneChipset, 1, 2) }, 10000, new ItemStack(bcplugins.blockMarker, 1)));
			AssemblyRecipe.assemblyRecipes
					.add(new AssemblyRecipe(new ItemStack[] { new ItemStack(BuildCraftFactory.pumpBlock, 1), new ItemStack(BuildCraftFactory.tankBlock, 32),
							new ItemStack(BuildCraftTransport.pipeFluidsGold, 2) }, 160000, new ItemStack(bcplugins.blockPump, 1)));
			AssemblyRecipe.assemblyRecipes.add(new AssemblyRecipe(new ItemStack[] { new ItemStack(BuildCraftFactory.refineryBlock, 1),
					new ItemStack(redstoneChipset, 1, 3), new ItemStack(BuildCraftFactory.hopperBlock, 2) }, 80000, new ItemStack(bcplugins.blockRefinery, 1)));
			AssemblyRecipe.assemblyRecipes.add(new AssemblyRecipe(new ItemStack[] { new ItemStack(BuildCraftCore.wrenchItem, 2),
					new ItemStack(BuildCraftCore.ironGearItem, 2), new ItemStack(BuildCraftSilicon.assemblyTableBlock, 1) }, 20000, new ItemStack(bcplugins.itemTool, 1,
					0)));
			AssemblyRecipe.assemblyRecipes.add(new AssemblyRecipe(new ItemStack[] { new ItemStack(BuildCraftCore.wrenchItem, 2),
					new ItemStack(Item.writableBook, 1), new ItemStack(Item.book, 16) }, 40000, new ItemStack(bcplugins.itemTool, 1, 1)));
			AssemblyRecipe.assemblyRecipes.add(new AssemblyRecipe(new ItemStack[] { new ItemStack(BuildCraftCore.wrenchItem, 2),
					new ItemStack(Item.bucketEmpty, 2), new ItemStack(Item.bucketWater, 1), new ItemStack(Item.bucketLava, 1) }, 160000, new ItemStack(
							bcplugins.itemTool, 1, 2)));
			AssemblyRecipe.assemblyRecipes.add(new AssemblyRecipe(new ItemStack[] { new ItemStack(BuildCraftFactory.miningWellBlock, 1),
					new ItemStack(redstoneChipset, 1, 3), new ItemStack(BuildCraftTransport.yellowPipeWire, 8) }, 80000, new ItemStack(bcplugins.blockMiningWell, 1)));
			AssemblyRecipe.assemblyRecipes.add(new AssemblyRecipe(new ItemStack[] { new ItemStack(Block.dispenser, 1), new ItemStack(Item.pickaxeDiamond, 1),
					new ItemStack(BuildCraftEnergy.engineBlock, 1, 1) }, 40000, new ItemStack(bcplugins.blockBreaker, 1)));
			AssemblyRecipe.assemblyRecipes
					.add(new AssemblyRecipe(new ItemStack[] { new ItemStack(Block.dispenser, 1), new ItemStack(BuildCraftBuilders.fillerBlock, 1),
							new ItemStack(BuildCraftEnergy.engineBlock, 1, 1) }, 80000, new ItemStack(bcplugins.blockPlacer, 1)));
			AssemblyRecipe.assemblyRecipes.add(new AssemblyRecipe(new ItemStack[] { new ItemStack(BuildCraftSilicon.laserBlock, 1),
					new ItemStack(BuildCraftTransport.pipePowerGold, 32), new ItemStack(Block.glass, 16) }, 160000, new ItemStack(bcplugins.blockLaser, 1)));
			break;
		case 3:
			AssemblyRecipe.assemblyRecipes.add(new AssemblyRecipe(new ItemStack[] { new ItemStack(BuildCraftFactory.quarryBlock, 2),
					new ItemStack(redstoneChipset, 8, 3), new ItemStack(BuildCraftTransport.yellowPipeWire, 16), new ItemStack(redstoneChipset, 2, 4),
					new ItemStack(Block.chest, 32) }, 800000, new ItemStack(bcplugins.blockQuarry, 1)));
			AssemblyRecipe.assemblyRecipes.add(new AssemblyRecipe(new ItemStack[] { new ItemStack(Block.enchantmentTable, 1),
					new ItemStack(BuildCraftSilicon.assemblyTableBlock, 1, 1), new ItemStack(Block.anvil, 2), new ItemStack(BuildCraftSilicon.laserBlock, 4),
					new ItemStack(BuildCraftSilicon.assemblyTableBlock, 1), new ItemStack(redstoneChipset, 4, 3),
					new ItemStack(BuildCraftCore.diamondGearItem, 2) }, 640000, new ItemStack(bcplugins.blockMover, 1)));
			AssemblyRecipe.assemblyRecipes.add(new AssemblyRecipe(new ItemStack[] { new ItemStack(BuildCraftBuilders.markerBlock, 1),
					new ItemStack(redstoneChipset, 4, 2), new ItemStack(redstoneChipset, 4, 3) }, 160000, new ItemStack(bcplugins.blockMarker, 1)));
			AssemblyRecipe.assemblyRecipes.add(new AssemblyRecipe(new ItemStack[] { new ItemStack(BuildCraftFactory.pumpBlock, 2),
					new ItemStack(BuildCraftFactory.tankBlock, 64), new ItemStack(BuildCraftTransport.pipeFluidsGold, 32),
					new ItemStack(BuildCraftFactory.quarryBlock, 1) }, 640000, new ItemStack(bcplugins.blockPump, 1)));
			AssemblyRecipe.assemblyRecipes.add(new AssemblyRecipe(new ItemStack[] { new ItemStack(BuildCraftFactory.refineryBlock, 2),
					new ItemStack(BuildCraftFactory.tankBlock, 16), new ItemStack(Block.anvil, 2), new ItemStack(BuildCraftSilicon.laserBlock, 4),
					new ItemStack(BuildCraftSilicon.assemblyTableBlock, 1), new ItemStack(redstoneChipset, 2, 3),
					new ItemStack(BuildCraftCore.diamondGearItem, 2) }, 640000, new ItemStack(bcplugins.blockRefinery, 1)));
			AssemblyRecipe.assemblyRecipes
					.add(new AssemblyRecipe(new ItemStack[] { new ItemStack(BuildCraftCore.wrenchItem, 4), new ItemStack(BuildCraftCore.diamondGearItem, 2),
							new ItemStack(BuildCraftSilicon.assemblyTableBlock, 1), new ItemStack(BuildCraftBuilders.fillerBlock, 1),
							new ItemStack(BuildCraftBuilders.markerBlock, 4) }, 160000, new ItemStack(bcplugins.itemTool, 1, 0)));
			AssemblyRecipe.assemblyRecipes.add(new AssemblyRecipe(new ItemStack[] { new ItemStack(BuildCraftCore.wrenchItem, 4),
					new ItemStack(Item.writableBook, 1), new ItemStack(Item.book, 64), new ItemStack(redstoneChipset, 8, 3) }, 320000, new ItemStack(bcplugins.itemTool,
					1, 1)));
			AssemblyRecipe.assemblyRecipes.add(new AssemblyRecipe(new ItemStack[] { new ItemStack(BuildCraftCore.wrenchItem, 4),
					new ItemStack(Item.bucketEmpty, 6), new ItemStack(Item.bucketWater, 1), new ItemStack(Item.bucketLava, 1),
					new ItemStack(BuildCraftEnergy.bucketOil, 1), new ItemStack(BuildCraftEnergy.bucketFuel, 1),
					new ItemStack(BuildCraftEnergy.engineBlock, 1, 2) }, 640000, new ItemStack(bcplugins.itemTool, 1, 2)));
			AssemblyRecipe.assemblyRecipes.add(new AssemblyRecipe(new ItemStack[] { new ItemStack(BuildCraftFactory.miningWellBlock, 2),
					new ItemStack(redstoneChipset, 8, 3), new ItemStack(BuildCraftTransport.yellowPipeWire, 16), new ItemStack(redstoneChipset, 1, 4),
					new ItemStack(Block.chest, 16) }, 500000, new ItemStack(bcplugins.blockMiningWell, 1)));
			AssemblyRecipe.assemblyRecipes.add(new AssemblyRecipe(new ItemStack[] { new ItemStack(Block.dispenser, 2), new ItemStack(Block.blockDiamond, 2),
					new ItemStack(Item.redstone, 64), new ItemStack(bcplugins.blockQuarry, 1), new ItemStack(bcplugins.blockMiningWell, 1),
					new ItemStack(BuildCraftEnergy.engineBlock, 16, 2) }, 640000, new ItemStack(bcplugins.blockBreaker, 1)));
			AssemblyRecipe.assemblyRecipes.add(new AssemblyRecipe(new ItemStack[] { new ItemStack(Block.dispenser, 2), new ItemStack(Block.blockDiamond, 2),
					new ItemStack(Item.redstone, 64), new ItemStack(BuildCraftBuilders.fillerBlock, 2), new ItemStack(Block.blockGold, 2),
					new ItemStack(BuildCraftEnergy.engineBlock, 16, 2) }, 1280000, new ItemStack(bcplugins.blockPlacer, 1)));
			AssemblyRecipe.assemblyRecipes.add(new AssemblyRecipe(new ItemStack[] { new ItemStack(BuildCraftSilicon.laserBlock, 16),
					new ItemStack(Block.glass, 64), new ItemStack(BuildCraftTransport.pipePowerGold, 64), new ItemStack(Item.glowstone, 64),
					new ItemStack(Block.obsidian, 16) }, 7654321, new ItemStack(bcplugins.blockLaser, 1)));
			break;
		default:
			AssemblyRecipe.assemblyRecipes.add(new AssemblyRecipe(new ItemStack[] { new ItemStack(BuildCraftFactory.quarryBlock, 1),
					new ItemStack(redstoneChipset, 2, 3), new ItemStack(BuildCraftTransport.yellowPipeWire, 16), new ItemStack(redstoneChipset, 1, 4),
					new ItemStack(Block.chest, 8) }, 320000, new ItemStack(bcplugins.blockQuarry, 1)));
			AssemblyRecipe.assemblyRecipes.add(new AssemblyRecipe(new ItemStack[] { new ItemStack(Block.enchantmentTable, 1),
					new ItemStack(BuildCraftSilicon.assemblyTableBlock, 1, 1), new ItemStack(Block.anvil, 1),
					new ItemStack(BuildCraftSilicon.assemblyTableBlock, 1), new ItemStack(redstoneChipset, 4, 3),
					new ItemStack(BuildCraftSilicon.laserBlock, 4) }, 320000, new ItemStack(bcplugins.blockMover, 1)));
			AssemblyRecipe.assemblyRecipes.add(new AssemblyRecipe(new ItemStack[] { new ItemStack(BuildCraftBuilders.markerBlock, 1),
					new ItemStack(redstoneChipset, 4, 2), new ItemStack(BuildCraftCore.wrenchItem, 1) }, 20000, new ItemStack(bcplugins.blockMarker, 1)));
			AssemblyRecipe.assemblyRecipes.add(new AssemblyRecipe(new ItemStack[] { new ItemStack(BuildCraftFactory.pumpBlock, 2),
					new ItemStack(BuildCraftFactory.tankBlock, 64), new ItemStack(BuildCraftTransport.pipeFluidsGold, 8),
					new ItemStack(BuildCraftTransport.pipeFluidsStone, 32) }, 320000, new ItemStack(bcplugins.blockPump, 1)));
			AssemblyRecipe.assemblyRecipes.add(new AssemblyRecipe(new ItemStack[] { new ItemStack(BuildCraftFactory.refineryBlock, 1),
					new ItemStack(BuildCraftFactory.tankBlock, 8), new ItemStack(Block.anvil, 1), new ItemStack(BuildCraftSilicon.laserBlock, 2),
					new ItemStack(BuildCraftSilicon.assemblyTableBlock, 1), new ItemStack(redstoneChipset, 1, 3),
					new ItemStack(BuildCraftCore.diamondGearItem, 1) }, 640000, new ItemStack(bcplugins.blockRefinery, 1)));
			AssemblyRecipe.assemblyRecipes.add(new AssemblyRecipe(new ItemStack[] { new ItemStack(BuildCraftCore.wrenchItem, 2),
					new ItemStack(BuildCraftCore.goldGearItem, 2), new ItemStack(BuildCraftSilicon.assemblyTableBlock, 1),
					new ItemStack(BuildCraftBuilders.markerBlock, 2) }, 80000, new ItemStack(bcplugins.itemTool, 1, 0)));
			AssemblyRecipe.assemblyRecipes.add(new AssemblyRecipe(new ItemStack[] { new ItemStack(BuildCraftCore.wrenchItem, 2),
					new ItemStack(Item.writableBook, 1), new ItemStack(Item.book, 32), new ItemStack(redstoneChipset, 2, 3) }, 160000, new ItemStack(bcplugins.itemTool,
					1, 1)));
			AssemblyRecipe.assemblyRecipes.add(new AssemblyRecipe(new ItemStack[] { new ItemStack(BuildCraftCore.wrenchItem, 2),
					new ItemStack(Item.bucketEmpty, 6), new ItemStack(Item.bucketWater, 1), new ItemStack(Item.bucketLava, 1),
					new ItemStack(BuildCraftEnergy.bucketOil, 1), new ItemStack(BuildCraftEnergy.bucketFuel, 1) }, 320000, new ItemStack(bcplugins.itemTool, 1, 2)));
			AssemblyRecipe.assemblyRecipes.add(new AssemblyRecipe(new ItemStack[] { new ItemStack(BuildCraftFactory.miningWellBlock, 1),
					new ItemStack(redstoneChipset, 2, 3), new ItemStack(BuildCraftTransport.yellowPipeWire, 16), new ItemStack(redstoneChipset, 1, 4),
					new ItemStack(Block.chest, 8) }, 160000, new ItemStack(bcplugins.blockMiningWell, 1)));
			AssemblyRecipe.assemblyRecipes.add(new AssemblyRecipe(new ItemStack[] { new ItemStack(Block.dispenser, 2), new ItemStack(Block.blockDiamond, 1),
					new ItemStack(Item.redstone, 32), new ItemStack(bcplugins.blockQuarry, 1), new ItemStack(BuildCraftEnergy.engineBlock, 1, 2) }, 320000,
					new ItemStack(bcplugins.blockBreaker, 1)));
			AssemblyRecipe.assemblyRecipes.add(new AssemblyRecipe(new ItemStack[] { new ItemStack(Block.dispenser, 2), new ItemStack(Block.blockDiamond, 1),
					new ItemStack(Item.redstone, 32), new ItemStack(BuildCraftBuilders.fillerBlock, 1), new ItemStack(Block.blockGold, 2),
					new ItemStack(BuildCraftEnergy.engineBlock, 1, 2) }, 640000, new ItemStack(bcplugins.blockPlacer, 1)));
			AssemblyRecipe.assemblyRecipes.add(new AssemblyRecipe(new ItemStack[] { new ItemStack(BuildCraftSilicon.laserBlock, 4),
					new ItemStack(Block.glass, 64), new ItemStack(BuildCraftTransport.pipePowerGold, 64), new ItemStack(Item.glowstone, 32) }, 1280000,
					new ItemStack(bcplugins.blockLaser, 1)));
		}
	}
	
	public void shaplessRecipes(){
		GameRegistry.addShapelessRecipe(new ItemStack(Block.cobblestone),Block.dirt,new ItemStack(bcplugins.PhilosopherStone,1,Short.MAX_VALUE));
		GameRegistry.addShapelessRecipe(new ItemStack(Block.dirt),Block.cobblestone,new ItemStack(bcplugins.PhilosopherStone,1,Short.MAX_VALUE));

	}
	
	public void shapedRecipes(){
        GameRegistry.addRecipe(new ItemStack(bcplugins.RedPickaxe), new Object[]{"RRR"," B "," B ",'R',Item.redstone,'B',Item.blazeRod});
        GameRegistry.addRecipe(new ItemStack(bcplugins.RedShovel), new Object[]{" R "," B "," B ",'R',Item.redstone,'B',Item.blazeRod});

	}
}
