package jgile2.mods.bcplugins.client;


import jgile2.mods.bcplugins.TileEntity.TileCharger;
import jgile2.mods.bcplugins.container.ContainerCharger;
import jgile2.mods.bcplugins.container.ContainerMover;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;

import org.lwjgl.opengl.GL11;

public class guiCharger extends GuiContainer
{
	private static final ResourceLocation gui = new ResourceLocation("bcplugins", "textures/gui/charger.png");
	public TileCharger te;
	public int x;
	public int y;
	public int z;
	public World world;

	public guiCharger(EntityPlayer player, World world, int x, int y, int z,TileCharger entity) {
		super(new ContainerCharger(player, world, x, y, z));
		this.inventorySlots = new ContainerCharger(player, world, x, y, z);
		te =entity;
		this.x=x;
		this.y=y;
		this.z=z;
		this.world = world;
	}

	@Override
	public void initGui() {
		super.initGui();
		int i = this.width - this.xSize >> 1;
		int j = this.height - this.ySize >> 1;
	
	}

	@Override
	public void drawGuiContainerForegroundLayer(int par1, int par2) {
		this.fontRenderer.drawString("Charger", 65, 7, 0x404040);

		this.fontRenderer.drawString(StatCollector.translateToLocal("container.inventory"), 8, 72, 0x404040);
	}

	@Override
	public void drawGuiContainerBackgroundLayer(float f, int i, int j) {
		TileCharger entity =(TileCharger) world.getBlockTileEntity(x, y, z);
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.mc.getTextureManager().bindTexture(gui);

		int l = this.width - this.xSize >> 1;
		int i1 = this.height - this.ySize >> 1;
		drawTexturedModalRect(l, i1, 0, 0, this.xSize, this.ySize);
		

		int k = this.te.powerScaled(55);
		//int k = te.power;
		
    	

		//drawTexturedModalRect(guiLeft+7, guiTop + 12, 176,0 + k, 14, 2+k);
		drawTexturedModalRect(guiLeft + 7, guiTop + 67 - k, 176,55-k, 14, k+1);

	}
}