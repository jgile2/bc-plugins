
package jgile2.mods.bcplugins.client;

import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.entity.Entity;
import net.minecraft.network.packet.Packet;
import net.minecraft.world.World;
import net.minecraftforge.client.MinecraftForgeClient;

import jgile2.mods.bcplugins.CommonProxy;
import jgile2.mods.bcplugins.EntityMechanicalArm;
import jgile2.mods.bcplugins.bcplugins;
import jgile2.mods.bcplugins.TileEntity.TileCharger;
import jgile2.mods.bcplugins.TileEntity.TileChargerRenderer;
import jgile2.mods.bcplugins.TileEntity.TileRefinery;

import buildcraft.BuildCraftTransport;
import buildcraft.core.render.RenderVoid;
import buildcraft.transport.TransportProxyClient;
import buildcraft.transport.render.PipeItemRenderer;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.relauncher.SideOnly;
import cpw.mods.fml.relauncher.Side;

@SideOnly(Side.CLIENT)
public class ClientProxy extends CommonProxy {
	public final static PipeItemRenderer pipeItemRenderer = new PipeItemRenderer();
	public static int BlockRenderID;

	
	@Override
	public void registerTextures() {
		RenderingRegistry.registerEntityRenderingHandler(EntityMechanicalArm.class, new RenderVoid());
		ClientRegistry.bindTileEntitySpecialRenderer(TileRefinery.class, RenderRefinery.INSTANCE);
		MinecraftForgeClient.registerItemRenderer(bcplugins.BlockPipe.itemID, pipeItemRenderer);
		RenderingRegistry.registerBlockHandler(RenderRefinery.INSTANCE);
		ClientRegistry.bindTileEntitySpecialRenderer(TileCharger.class, new TileChargerRenderer());
        
       
	}

	@Override
	public void removeEntity(Entity e) {
		e.worldObj.removeEntity(e);
		if (e.worldObj.isRemote) ((WorldClient) e.worldObj).removeEntityFromWorld(e.entityId);
	}

	@Override
	public World getClientWorld() {
		return Minecraft.getMinecraft().theWorld;
	}
		 
}