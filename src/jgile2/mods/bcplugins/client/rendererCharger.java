package jgile2.mods.bcplugins.client;

import net.minecraft.client.renderer.entity.Render;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

public class rendererCharger extends Render{

	private modelCharger model;
	
	public rendererCharger(){
		model = new modelCharger();
		shadowSize = 0.2F;
	}
	
	 private static final ResourceLocation texture = new ResourceLocation("bcplugins", "textures/gui/charger.png");
	   
	 @Override
	public void doRender(Entity entity, double x, double y, double z,float yaw, float patialTickTime) {
		GL11.glPushMatrix();
		GL11.glTranslatef((float)x,(float) y,(float) z);
		GL11.glScalef(-1F, -1F, -1F);
		
		bindEntityTexture(entity);
		model.render(entity, 0, 0, 0, 0, 0, 0.0625F);
		
		GL11.glPopMatrix();
	}

	@Override
	protected ResourceLocation getEntityTexture(Entity entity) {
		return texture;
	}

}
