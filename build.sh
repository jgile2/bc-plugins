#!/bin/bash
MCVERSION="1.6.4"
MAJOR="0"
MINOR="0"
MINORMINOR="1"

if [ -d "builds" ]; then
  rm -r builds
fi
mkdir builds
cp -ar $JENKINS_HOME/forge $WORKSPACE
mkdir $WORKSPACE/forge/mcp/src/minecraft/
cp -ar $WORKSPACE/src/ $WORKSPACE/forge/mcp/src/minecraft/
cd $WORKSPACE/forge/mcp/
sh recompile.sh
sh reobfuscate_srg.sh
cd $WORKSPACE/forge/mcp/reobf/minecraft
mkdir $WORKSPACE/forge/mcp/reobf/minecraft/bcplugins/assets
cp -ar $WORKSPACE/src/assets $WORKSPACE/forge/mcp/reobf/minecraft/
cp -ar $WORKSPACE/src/mcmod.info $WORKSPACE/forge/mcp/reobf/minecraft/
zip -r -D -9 $WORKSPACE/builds/bcplugins_$MCVERSION-$MAJOR.$MINOR.$MINORMINOR.b$BUILD_NUMBER.zip *
